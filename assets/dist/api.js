(function ($) {
	var request_page = 1,
		$widget = $("[data-shp-widget-medienjobs]");

	var getPage = function ($target, url) {
		$.ajax({
			cache: false,
			dataType: "json",
			url: url,
			data: { page: request_page },
			success: function (response_data) {
				$target.removeClass("is--waiting");
				if (response_data.rendered) {
					$target.html(response_data.rendered);
					if ($target.html() === "") {
						$target.addClass("is--empty");
					} else {
						$target.addClass("is--filled");
					}
					initClicks();
					// $(window).trigger('sht/move-posts-around');
					if (response_data.css_class) {
						$target.addClass(response_data.css_class);
					}
				}
			},
			error: function () {
				$target.removeClass("is--waiting");
				$target.addClass("response-error");
			},
		});
	};

	var initClicks = function () {
		$widget.find("[data-pagination-goto]").click(function () {
			request_page = $(this).data("pagination-goto");
			getPage(
				$(this).closest("[data-shp-widget-medienjobs]"),
				$(this)
					.closest("[data-shp-widget-medienjobs-url]")
					.data("shp-widget-medienjobs-url")
			);
		});
	};

	$widget.each(function () {
		var $this = $(this);
		$this.addClass("is--dynamic");
		var url = $(this).data("shp-widget-medienjobs-url");
		var validation_code = $(this).data("api-validation-code");
		if (validation_code !== "" && url !== "") {
			$(this).addClass("is--waiting");
			getPage($this, url);
		}
	});
})(jQuery);
