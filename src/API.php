<?php

namespace SayHello\Plugin\WidgetMedienjobs;

use WP_REST_Server;
use WP_REST_Request;
use WP_REST_Response;
use WP_REST_Controller;
use WP_Error;

class API extends WP_REST_Controller
{

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes()
	{
		$namespace = 'wp/v2';
		$base      = '/widget';

		register_rest_route(
			$namespace,
			$base,
			array(
				'methods'              => WP_REST_Server::READABLE,
				'callback'             => array($this, 'get_registered_items'),
				'permission_callback'  => array($this, 'permissions_check'),
			)
		);

		register_rest_route(
			$namespace,
			$base . '/(?P<id_base>[^/]+)',
			array(
				array(
					'methods'              => WP_REST_Server::READABLE,
					'callback'             => array($this, 'get_items'),
					'permissions_callback' => array($this, 'permissions_check'),
				)
			)
		);

		register_rest_route(
			$namespace,
			$base . '/(?P<id_base>[^/]+)/(?P<widget_number>[\d]+)',
			array(
				array(
					'methods'              => WP_REST_Server::READABLE,
					'callback'             => array($this, 'get_item'),
					'permissions_callback' => array($this, 'permissions_check'),
				)
			)
		);
	}

	/**
	 * Get a collection of items.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_registered_items($request)
	{
		global $wp_widget_factory;

		$data = array();
		foreach ($wp_widget_factory->widgets as $key => $item) {
			$item->class = $key;
			$itemdata    = new WP_REST_Response($item);
			$itemdata->add_link(
				'registered-widgets',
				rest_url('wp/v2/widget/' . $item->id_base),
				array('embeddable' => true)
			);
			$data[] = $this->prepare_response_for_collection($itemdata);
		}

		return rest_ensure_response($data);
	}

	/**
	 * Get subcollection.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items($request)
	{
		$id_base = $request->get_param('id_base');

		$widget_options = get_option('widget_' . $id_base);
		unset($widget_options['_multiwidget']);

		$data = [];
		foreach ($widget_options as $number => $item) {
			if ($number == 1) {
				continue;
			}

			$item = $this->get_widget_item($id_base, $number, $request);
			if (!$item) {
				return new WP_Error(404);
			}

			$itemdata = new WP_REST_Response($item);
			$itemdata->add_link(
				'self',
				rest_url('wp/v2/widget/' . $id_base . '/' . $number)
			);
			$data[] = $this->prepare_response_for_collection($itemdata);
		}

		return rest_ensure_response([
			'code' => 'contains_rss',
			'data' => $data
		]);
	}

	/**
	 * Get one item from the collection.
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 *
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_item($request)
	{
		$id_base = $request->get_param('id_base');
		$number  = absint($request->get_param('widget_number'));

		$item = $this->get_widget_item($id_base, $number, $request);
		if (!$item) {
			return new WP_Error(404);
		}

		$itemdata = new WP_REST_Response($item);
		$itemdata->add_link(
			'self',
			rest_url('wp/v2/widget/' . $id_base . '/' . $number)
		);

		return $itemdata;
	}

	protected function get_widget_item($id_base, $number, $request)
	{
		global $wp_registered_widgets;

		if (!isset($wp_registered_widgets[$id_base . '-' . $number])) {
			return false;
		}

		$widget = $wp_registered_widgets[$id_base . '-' . $number];
		$widget['id_base'] = $id_base;
		$widget['number'] = $number;
		$widget['current_page'] = max(1, (int) $request->get_param('page'));
		$widget['options'] = get_fields('widget_' . $widget['id_base'] . '-' . $number);
		$widget['rendered'] = '';
		$widget['plugin_version'] = shp_widget_medienjobs_get_instance()->version;

		$feed = fetch_feed($widget['options']['feed_url']);

		$widget['rendered'] = shp_widget_medienjobs_get_instance()->renderItems($feed, $widget);

		unset($widget['callback']);

		return $widget;
	}

	/**
	 * Check if a given request has access.
	 *
	 * @return WP_Error|bool
	 */
	public function permissions_check_all()
	{
		return current_user_can('edit_theme_options');
	}

	/**
	 * Check if a given request has access.
	 *
	 * @return WP_Error|bool
	 */
	public function permissions_check()
	{
		return true;
	}
}
