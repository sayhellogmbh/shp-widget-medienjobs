<?php

/**
 * FUnction names are important because they have to match the base WP_Widget class
 * @var [type]
 */

class WP_Widget_Medienjobs extends WP_Widget
{

	/**
	 * Sets up a new RSS widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct()
	{
		$widget_ops  = array(
			'description' => __('Entries from any RSS or Atom feed.'),
			'customize_selective_refresh' => true,
		);
		$control_ops = array(
			'width'  => 400,
			'height' => 200,
		);
		parent::__construct('shp-medienjobswidget', __('RSS MedienJobs'), $widget_ops, $control_ops);
	}

	/**
	 * Outputs the content for the current RSS widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current RSS widget instance.
	 */
	public function widget($args, $instance)
	{

		preg_match('~shp-medienjobswidget-([0-9]+)~', $args['widget_id'], $matches);

		if (!is_array($matches) || !sizeof($matches) || !(int) $matches[1]) {
			return;
		}

		$fields = (array) get_fields('widget_' . $args['widget_id']);

		if (empty($fields['feed_url'] ?? '')) {
			return;
		}

		$title = $fields['title'] ?? '';
		if (empty($title)) {
			$rss = fetch_feed($fields['feed_url']);
			$title = strip_tags($rss->get_title());
		}

		$rest_url = sprintf(
			'%swp/v2/widget/shp-medienjobswidget/%s',
			get_rest_url(),
			$matches[1]
		);

		$logo = $fields['logo'] ?? null;

		if (!empty($logo)) {
			$logo_html = sprintf(
				'<img alt="%s" src="%s" class="shp-widget-medienjobs__logo">',
				$logo['alt'],
				$logo['url']
			);

			if (!empty($logo_link = $fields['logo_link'] ?? null)) {
				$logo_html = sprintf(
					'<a class="shp-widget-medienjobs__logolink" href="%s" rel="sponsored">%s</a>',
					$logo_link,
					$logo_html
				);
			}

			$args['before_widget'] .= $logo_html;
		}

		printf(
			'%s<p class="shp-widget-medienjobs__title special-small-title">%s</p><div class="shp-widget-medienjobs__content" data-shp-widget-medienjobs data-shp-widget-medienjobs-url="%s" api-validation-code="%s"><a target="_blank" class="shp-widget-medienjobs__link" href="%s">RSS Feed</a></div>%s',
			$args['before_widget'],
			$title,
			$rest_url,
			'contains_rss',
			$fields['feed_url'],
			$args['after_widget']
		);

		return;
	}

	/**
	 * Handles updating settings for the current RSS widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	public function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	/**
	 * Outputs the settings form for the RSS widget.
	 *
	 * @since 2.8.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form($instance)
	{
	}
}
