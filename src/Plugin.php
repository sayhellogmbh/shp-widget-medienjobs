<?php

namespace SayHello\Plugin\WidgetMedienjobs;

//use SimplePie_Category;

class Plugin
{
	private static $instance;
	public $name = '';
	public $prefix = '';
	public $version = '';
	public $file = '';
	public $api = null;
	public $per_page = 0;

	public static function getInstance($file)
	{
		if (!isset(self::$instance) && !(self::$instance instanceof Plugin)) {
			self::$instance = new Plugin;

			if (!function_exists('get_plugin_data')) {
				include_once(ABSPATH . 'wp-admin/includes/plugin.php');
			}

			$data = get_plugin_data($file);

			self::$instance->name = $data['Name'];
			self::$instance->prefix = 'shp-widget-medienjobs';
			self::$instance->version = $data['Version'];
			self::$instance->file = $file;

			self::$instance->run();
		}
		return self::$instance;
	}

	private function run()
	{
		add_action('plugins_loaded', [$this, 'loadPluginTextdomain']);
		add_action('widgets_init', [$this, 'registerWidget'], 1);
		add_action('rest_api_init', [$this, 'restEndpoints']);
		add_action('wp_enqueue_scripts', [$this, 'addScripts']);
		add_action('acf/init', [$this, 'widgetFields']);
		// add_action('wp_feed_options', [$this, 'feedNoCache']);
	}

	public function loadPluginTextdomain()
	{
		load_plugin_textdomain('shp-widget-medienjobs', false, dirname(plugin_basename($this->file)) . '/languages');
	}

	public function registerWidget()
	{
		include_once(__DIR__ . '/MedienJobsWidget.php');
		register_widget('WP_Widget_Medienjobs');
	}

	public function feedNoCache(&$feed)
	{
		if (strpos($feed->feed_url, 'medienjobs.ch') !== false) {
			$feed->enable_cache(false);
		}
	}

	public function renderItems($feed, $widget)
	{
		if (is_wp_error($feed)) {
			//wp_mail('simea@haemeulrich.com,mark@sayhello.ch', 'Medienjobs Feed - Error',$feed->get_error_message());
			return 'Der Inhalt dieses Feeds ist leider ungültig. Wir sind dran!';
		}

		$per_page = max((int) $widget['options']['number_of_entries'], 1);
		$max_pages = max((int) $widget['options']['number_of_pages'], 1);
		$current_page = max((int) $widget['current_page'], 1);
		$offset = max(0, (($current_page - 1) * $per_page));

		$all_feed_items = $feed->get_items();
		$all_usable_items = [];

		foreach ($all_feed_items as $all_feed_item) {
			// Use the following code to constrain the selection to items from a specific category
			// $category = $all_feed_item->get_category();
			// if (!$category instanceof SimplePie_Category || !$category->term || ($category->term !== 'Grafik / Prepress / Druck / Verpackung / Werbetechnik' && $category->term !== 'Webpublishing')) {
			// 	continue;
			// }
			$all_usable_items[] = $all_feed_item;
		}

		$paginated_items = array_slice($all_usable_items, $offset, $per_page);

		if (empty($paginated_items)) {
			return '';
		}

		if (is_wp_error($feed)) {
			return '';
		}

		$list = [];
		$html = '';

		ob_start();
		foreach ($paginated_items as $item) {
			$firma = $item->get_item_tags(null, 'firma')[0]['data'] ?? null;
			$arbeitsort = $item->get_item_tags(null, 'arbeitsort')[0]['data'] ?? null;
			$list[] = sprintf(
				'<li class="shp-widget-medienjobs__entry"><p class="shp-widget-medienjobs__entrytitle"><a class="shp-widget-medienjobs__entrylink" href="%s" rel="sponsored" target="_blank">%s</a></p>%s%s</li>',
				$item->get_permalink(),
				strip_tags($item->get_title()),
				!empty($firma . $arbeitsort) ? '<p class="shp-widget-medienjobs__place">' . implode(', ', [$firma, $arbeitsort]) . '</p>' : '',
				'<time class="shp-widget-medienjobs__date">' . sprintf(
					'Veröffentlicht am %s',
					date_i18n(get_option('date_format'), strtotime($item->get_date()))
				) . '</time>'
			);
		}

		$pagination = $this->pagination([
			'totalPosts' => count($all_usable_items),
			'postsToShow' => $per_page,
			'current_page' => $current_page,
			'max_pages' => $max_pages,
		]);

		if (!empty($list)) {
			echo '<!-- shp_widget_medienjobs - HTML built ' . wp_date('r') . ' --><ul class="shp-widget-medienjobs__entries">' . implode('', $list) . '</ul>' . $pagination;
		}

		$html .= ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public function restEndpoints()
	{
		include_once __DIR__ . '/API.php';
		$this->api = new API();
		$this->api->register_routes();
	}

	public function addScripts()
	{
		wp_enqueue_script('shp-widget-medienjobs', plugins_url('assets/dist/api.js', shp_widget_medienjobs_get_instance()->file), ['jquery'], filemtime(plugin_dir_path(shp_widget_medienjobs_get_instance()->file) . 'assets/dist/api.js'), true);
		wp_enqueue_style('shp-widget-medienjobs', plugins_url('assets/dist/style.css', shp_widget_medienjobs_get_instance()->file), [], filemtime(plugin_dir_path(shp_widget_medienjobs_get_instance()->file) . 'assets/dist/style.css'));
	}

	private function pagination($attributes)
	{

		$number_of_pages = (int) ceil($attributes['totalPosts'] / $attributes['postsToShow']);

		if (!$number_of_pages) {
			return '';
		}

		if ($number_of_pages > (int) $attributes['max_pages']) {
			$number_of_pages = $attributes['max_pages'];
		}

		$pages = [];

		$base = get_permalink();
		$base_pagination = $base . _x('paginierung', 'Custom rewrite endpoint', 'sht');

		for ($i = 1; $i <= $number_of_pages; $i++) {
			$link_base = $i > 1 ? $base_pagination : $base;

			if ($i === $attributes['current_page']) {
				$pages[] = sprintf(
					'<span class="shp-widget-medienjobs__paginationentry"><span aria-current="page" class="shp-widget-medienjobs__paginationlink current">%s</span></span>',
					$i
				);
			} else {
				$pages[] = sprintf(
					'<span class="shp-widget-medienjobs__paginationentry"><a class="shp-widget-medienjobs__paginationlink" data-pagination-goto="%1$s">%1$s</a></span>',
					$i
				);
			}
		}

		return sprintf(
			'<div class="shp-widget-medienjobs__pagination">
				<div class="shp-widget-medienjobs__paginationcontent">
					%s
				</div>
			</div>',
			implode('', $pages)
		);
	}

	public function widgetFields()
	{
		if (function_exists('acf_add_local_field_group')) :
			acf_add_local_field_group(array(
				'key' => 'shp-widget-medienjobs_options',
				'title' => __('Widget: RSS MedienJobs', 'shp-widget-medienjobs'),
				'fields' => array(
					array(
						'key' => 'shp-widget-medienjobs_title',
						'label' => __('Titel', 'shp-widget-medienjobs'),
						'name' => 'title',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'shp-widget-medienjobs_logo',
						'label' => __('Logo', 'shp-widget-medienjobs'),
						'name' => 'logo',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '.svg,.png',
					),
					array(
						'key' => 'shp-widget-medienjobs_logo_link',
						'label' => __('Link URL für Logo', 'shp-widget-medienjobs'),
						'name' => 'logo_link',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
					),
					array(
						'key' => 'shp-widget-medienjobs_feed_url',
						'label' => __('RSS URL', 'shp-widget-medienjobs'),
						'name' => 'feed_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
					),
					array(
						'key' => 'shp-widget-medienjobs_number_of_entries',
						'label' => __('Anzahl Einträge in der Liste', 'shp-widget-medienjobs'),
						'name' => 'number_of_entries',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => 100,
						'step' => '',
					),
					array(
						'key' => 'shp-widget-medienjobs_number_of_pages',
						'label' => __('Anzahl Seiten in der Paginierung', 'shp-widget-medienjobs'),
						'name' => 'number_of_pages',
						'type' => 'number',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 5,
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 1,
						'max' => 100,
						'step' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'widget',
							'operator' => '==',
							'value' => 'shp-medienjobswidget',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));
		endif;
	}
}
