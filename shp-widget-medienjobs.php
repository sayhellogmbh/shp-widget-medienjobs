<?php

/**
 * Plugin Name:     Widget: RSS Feed for medienjobs.ch
 * Plugin URI:      https://github.com/SayHelloGmbH/shp-widget-medienjobs
 * Description:     Provide a custom Widget for MedienJobs RSS feed.
 * Author:          Say Hello GmbH
 * Author URI:      https://sayhello.ch/?utm_source=wp-plugin&utm_medium=wp-plugin&utm_campaign=shp-widget-medienjobs
 * Text Domain:     shp-widget-medienjobs
 * Domain Path:     /languages
 * Version:         0.4.1
 */

namespace SayHello\Plugin\WidgetMedienjobs;

if (version_compare(get_bloginfo('version'), '5.3', '<') || version_compare(PHP_VERSION, '7.1', '<')) {
	function shp_widget_medienjobs_compatability_warning()
	{
		echo '<div class="error"><p>' . sprintf(
			__('“%1$s” requires PHP %2$s (or newer) and WordPress %3$s (or newer) to function properly. Your site is using PHP %4$s and WordPress %5$s. Please upgrade. The plugin has been automatically deactivated.', 'shp_widget_medienjobs'),
			__('RSS Feed for medienjobs.ch', 'shp-widget-medienjobs'),
			'7.1',
			'5.3',
			PHP_VERSION,
			$GLOBALS['wp_version']
		) . '</p></div>';
		if (isset($_GET['activate'])) {
			unset($_GET['activate']);
		}
	}
	add_action('admin_notices', 'shp_widget_medienjobs_compatability_warning');

	function shp_widget_medienjobs_deactivate_self()
	{
		deactivate_plugins(plugin_basename(__FILE__));
	}
	add_action('admin_init', 'shp_widget_medienjobs_deactivate_self');

	return;
} else {
	require_once 'src/Plugin.php';

	function shp_widget_medienjobs_get_instance()
	{
		return Plugin::getInstance(__FILE__);
	}
	shp_widget_medienjobs_get_instance();
}
